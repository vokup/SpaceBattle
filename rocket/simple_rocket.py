from base import Rocket

SIMPLE_ROCKET_SPEED = 500

class SimpleRocket(Rocket):
        def __init__(self, world, x, y):
            super().__init__("res/simple_rocket.png", world, x, y)

        def update(self):
            self.change_x = SIMPLE_ROCKET_SPEED * self.world.delta_time
            super().update()