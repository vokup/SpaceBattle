from base import Rocket

DOUBLE_ROCKET_SPEED = 500

class DoubleRocket(Rocket):
        def __init__(self, world, x, y):
            super().__init__("res/double_rocket.png", world, x, y)
        
        def update(self):
            self.change_x = DOUBLE_ROCKET_SPEED * self.world.delta_time
            super().update()