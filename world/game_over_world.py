import arcade
import math
from base import World, Text, Database

class GameOverWorld(World):
    def __init__(self, window, width, height, score):
        super().__init__(window, width, height, height, 0)
        db_score = Database.read_score()
        if score >= db_score:
            self.add_sprite(Text("Game Over!", self.width // 2, self.height // 2 + 100, 72))
            self.add_sprite(Text("Score: {0:04d}".format(int(score)), self.width // 2, self.height // 2, 36))
            self.add_sprite(Text("You are reaching HIGH SCORE!".format(int(score)), self.width // 2, self.height // 2 - 100, 42))
            self.add_sprite(Text("Press ENTER to play again.".format(int(score)), self.width // 2, self.height // 2 - 200, 36))
            Database.write_score(int(math.ceil(score)))
        else:
            self.add_sprite(Text("Game Over!", self.width // 2, self.height // 2 + 100, 72))
            self.add_sprite(Text("Score: {0:04d}".format(int(score)), self.width // 2, self.height // 2, 36))

    def update(self, delta_time):
        super().update()

    def on_key_press(self, symbol, modofiers):
        print("EFD")
        if symbol == arcade.key.ENTER:
            self.window.request_play_world()
        