import arcade
import random
from base import World, Timer, Story, Text
from model import Player, Hearth, Upgrade
from enemy import EnemyEasy, EnemyNormal, EnemyHard
from rocket import SimpleRocket, DoubleRocket

ROCKET_TIME_INTERVAL = 0.5
DOUBLE_ROCKET_ALIVE_TIME = 10
MAX_ENEMY_EASY = 5
MAX_ENEMY_NORMAL = 3
MAX_ENEMY_HARD = 2
MAX_HEARTH = 5

class PlayWorld(World):

    def __init__(self, window, width, height):
        super().__init__(window, width, height, height - 40, 0)
        self.is_game_over = False
        self.is_mouse_pressed = False
        self.rocket_last_time = 0
        self.delta_time = 0
        self.elapsed = 0
        self.total_enemy_count = 0
        self.enemy_easy_count = 0
        self.enemy_normal_count = 0
        self.enemy_hard_count = 0
        self.score = 0

        '''
        FOR DOUBLE ROCKET
        '''
        self.is_player_using_double_rocket = False
        self.double_rocket_init_time = 0

        self.enemy_list = arcade.SpriteList()
        self.enemy_easy_list = arcade.SpriteList()
        self.rocket_list = arcade.SpriteList()
        self.enemy_bullet_list = arcade.SpriteList()
        self.hearth_list = arcade.SpriteList()

        self.player = Player(self, 160, 360)
        self.add_sprite(self.player)
        self.register_mouse_motion(self.player)
        self.register_mouse_press(self.player)
        self.score_sprite = Text("SCORE: 0000", self.width - 100, self.height - 24, 24)
        self.add_sprite(self.score_sprite)
        self.add_sprite(Text("HP:", 36, self.height - 24, 24))

        for i in range(MAX_HEARTH):
            self.hearth_list.append(Hearth(80 + (i * 30), self.height - 26))
            self.add_sprite(self.hearth_list[-1])
        self.player_hearth = MAX_HEARTH

        self.upgrade_sprite = None
        #self.add_enemey(EnemyEasy(self, 1300, 360))
        #self.add_enemey(EnemyNormal(self, 1200, 360))
        #self.add_enemey(EnemyHard(self, 1200, 360))

        self.story_reader = Story(1)
        
    def add_rocket(self, sprite):
        self.rocket_list.append(sprite)
        self.add_sprite(sprite)

    def generate_easy_enemy(self, time):
        enemy = EnemyEasy(self, 1400, 0, time)
        valid_position = [ i for i in range(5) ]
        # cx = 85 + 170 * i
        for x in self.enemy_easy_list:
            valid_position.remove((x.center_y - 68) // 136)
    
        choose_position = valid_position[random.randint(0, len(valid_position) - 1)]
        enemy.center_y = 68 + 136 * choose_position
        return enemy

    def add_enemey(self, sprite):
        self.enemy_list.append(sprite)
        self.add_sprite(sprite)

        if isinstance(sprite, EnemyEasy):
            self.enemy_easy_count += 1
            self.enemy_easy_list.append(sprite)
        elif isinstance(sprite, EnemyNormal):
            self.enemy_normal_count += 1
        elif isinstance(sprite, EnemyHard):
            self.enemy_hard_count += 1
        self.total_enemy_count += 1

    def request_enemy_kill(self, enemy):
        time_calc = (enemy.init_time + enemy.alive_time - self.elapsed) / enemy.alive_time
        if time_calc < 0:
            time_calc = 0

        if isinstance(enemy, EnemyEasy):
            self.enemy_easy_count -= 1
            self.score += time_calc * 200
        elif isinstance(enemy, EnemyNormal):
            self.enemy_normal_count -= 1
            self.score += time_calc * 400
        elif isinstance(enemy, EnemyHard):
            self.enemy_hard_count -= 1
            self.score += time_calc * 600
        self.total_enemy_count -= 1

    def request_next_scene(self):
        self.total_enemy_count = 0

    def add_enemy_bullet(self, sprite):
        self.enemy_bullet_list.append(sprite)
        self.add_sprite(sprite)

    def increase_player_hearth(self):
        if self.player_hearth < 5:
            self.hearth_list[self.player_hearth].is_drawing = True
            self.player_hearth += 1

    def decrease_player_hearth(self):
        self.player_hearth -= 1
        self.hearth_list[self.player_hearth].is_drawing = False
        if self.player_hearth <= 0:
            self.player.kill()
            self.is_game_over = True

    '''
    GAME LOGIC IS HERE
    '''
    def update(self, delta_time):
        # must be the frist line of function
        self.delta_time = delta_time
        self.elapsed += delta_time

        if self.is_game_over:
            self.window.request_game_over_world(self.score)
            return

        if self.total_enemy_count == 0:
            story = self.story_reader.read()
            if story is None:
                self.is_game_over = True
                return
            print(vars(story))
            if story.type == Story.Data.TYPE_ENEMY:
                for data in story.data:
                    if data[0] == Story.Data.ENEMY_EASY:
                        for i in range(data[1]):
                            if self.enemy_easy_count >= MAX_ENEMY_EASY:
                                break
                            self.add_enemey(self.generate_easy_enemy(story.time))
                    elif data[0] == Story.Data.ENEMY_NORMAL:
                        for i in range(data[1]):
                            if self.enemy_normal_count >= MAX_ENEMY_NORMAL:
                                break
                            enemy = EnemyNormal(self, 1400, 0, story.time)
                            enemy.center_y = random.randint(enemy.width // 2, self.height - enemy.width // 2)
                            self.add_enemey(enemy)
                    elif data[0] == Story.Data.ENEMY_HARD:
                        for i in range(data[1]):
                            if self.enemy_hard_count >= MAX_ENEMY_HARD:
                                break
                            enemy = EnemyHard(self, 1400, 0, story.time)
                            enemy.center_y = random.randint(enemy.width // 2, self.height - enemy.width // 2)
                            self.add_enemey(enemy)
            elif story.type == Story.Data.TYPE_UPGRADE:
                if story.data == Story.Data.UPGRADE_ROCKET:
                    self.upgrade_sprite = Upgrade(self, Upgrade.ROCKET)
                elif story.data == Story.Data.UPGRADE_HEARTH:
                    self.upgrade_sprite = Upgrade(self, Upgrade.HEARTH)
                elif story.data == Story.Data.UPGRADE_RANDOM:
                    self.upgrade_sprite = Upgrade(self, Upgrade.RANDOM)
                self.add_sprite(self.upgrade_sprite)
                self.total_enemy_count = 1 # to waiting

        super().update()
        
        self.score_sprite.set_text("SCORE: {0:04d}".format(int(self.score)))
        '''
        CHECKING FOR COLLISION BEGIN HERE
        '''
        for bullet in self.enemy_bullet_list:
            if arcade.check_for_collision(self.player, bullet):
                self.decrease_player_hearth()
                bullet.kill()
                return None

        for enemy in self.enemy_list:
            if arcade.check_for_collision(self.player, enemy):
                self.decrease_player_hearth()
                enemy.kill()
                return None

        for rocket in self.rocket_list:
            for enemy in self.enemy_list:
                if arcade.check_for_collision(rocket, enemy):
                    enemy.kill()
                    rocket.kill()
                    return None

        if not self.upgrade_sprite is None:
            if arcade.check_for_collision(self.player, self.upgrade_sprite):
                self.upgrade_sprite.kill()
                if self.upgrade_sprite.upgrade_type == Upgrade.HEARTH:
                    self.increase_player_hearth()
                if self.upgrade_sprite.upgrade_type == Upgrade.ROCKET:
                    self.is_player_using_double_rocket = True
                    self.double_rocket_init_time = self.elapsed
                self.upgrade_sprite = None

        '''
        CHECKING FOR COLLISION END HERE
        '''

        if self.is_mouse_pressed:
            if self.elapsed - self.double_rocket_init_time >= DOUBLE_ROCKET_ALIVE_TIME:
                self.is_player_using_double_rocket = False

            if self.elapsed - self.rocket_last_time >= ROCKET_TIME_INTERVAL:
                if self.is_player_using_double_rocket:
                    self.add_rocket(DoubleRocket(self, self.player.center_x + 120, self.player.center_y - 10))
                else:
                    self.add_rocket(SimpleRocket(self, self.player.center_x + 120, self.player.center_y - 10))
                self.rocket_last_time = self.elapsed

    def on_mouse_press(self, x, y, button, modifiers):
        self.is_mouse_pressed = True

        if self.elapsed - self.double_rocket_init_time >= DOUBLE_ROCKET_ALIVE_TIME:
            self.is_player_using_double_rocket = False

        if self.elapsed - self.rocket_last_time >= ROCKET_TIME_INTERVAL:
            if self.is_player_using_double_rocket:
                self.add_rocket(DoubleRocket(self, self.player.center_x + 120, self.player.center_y - 10))
            else:
                self.add_rocket(SimpleRocket(self, self.player.center_x + 120, self.player.center_y - 10))
            self.rocket_last_time = self.elapsed

    def on_mouse_release(self, x, y, button, modifiers):
        self.is_mouse_pressed = False