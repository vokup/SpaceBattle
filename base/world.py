import arcade 

class World:
    def __init__(self, window, width, height, top_limit = 0, bottom_limit = 0):
        self.window = window
        self.width = width
        self.height = height
        self.top_limit = top_limit
        self.bottom_limit = bottom_limit
        self.sprite_list = arcade.SpriteList()
        self.mouse_motion_list = arcade.SpriteList()
        self.mouse_press_list =  arcade.SpriteList()
        self.mouse_release_list = arcade.SpriteList()

    def add_sprite(self, sprite):
        self.sprite_list.append(sprite)

    def clear_sprite_list(self):
        self.sprite_list = arcade.SpriteList()

    def update(self):
        for sprite in self.sprite_list:
            sprite.update()

    def on_draw(self):
        for sprite in self.sprite_list:
            sprite.draw()

    def on_mouse_press(self, x, y, button, modifiers):
        for sprite in self.mouse_press_list:
            sprite.on_mouse_press(x, y, button, modifiers)

    def on_mouse_release(self, x, y, button, modifiers):
        for sprite in self.mouse_release_list:
            sprite.on_mouse_release(x ,y , button, modifiers)

    def on_mouse_motion(self, x, y, dx, dy):
        for sprite in self.mouse_motion_list:
            sprite.on_mouse_motion(x, y, dx, dy)

    def on_key_press(self, symbol, modofiers):
        pass

    def on_key_release(self, symbol, modofiers):
        pass

    def register_mouse_motion(self, sprite):
        self.mouse_motion_list.append(sprite)

    def register_mouse_press(self, sprite):
        self.mouse_press_list.append(sprite)

    def register_mouse_release(self, sprite):
        self.mouse_release_list.append(sprite)