import arcade
from pyglet.text import Label

class Text(arcade.Sprite):
    def __init__(self, text, x, y, font_size):
        super().__init__(None)
        self.center_x = x
        self.center_y = y
        self.font_size = font_size
        self.handle = Label(text, 
                            ('Calibri', 'Arial'),
                            font_size, False, False,
                            (255, 255, 255, 255),
                            0, 0, None, None, "center", "center")

    def set_text(self, text):
        self.handle.text = text

    def draw(self):
        arcade.render_text(self.handle, self.center_x, self.center_y)