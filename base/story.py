class Story:

    class Data:
        TYPE_ENEMY = 0
        TYPE_UPGRADE = 1
        ENEMY_EASY = 0
        ENEMY_NORMAL = 1
        ENEMY_HARD = 2
        UPGRADE_ROCKET = 0
        UPGRADE_HEARTH = 1
        UPGRADE_RANDOM = 2

        def __init__(self):
            self.type = None
            self.data = None
            self.time = None

    def __init__(self, story_no):
        self.file_handle = open('story/{0}.story'.format(story_no))

    def reopen(self):
        self.file_handle.seek(0)

    @staticmethod
    def _is_int(value):
        try:
            int(value)
            return True
        except ValueError:
            return False

    def read(self):
        rec_data = Story.Data()

        line = self.file_handle.readline()
        data = line.split()
        data_len = len(data)

        if data_len <= 0:
            return None

        if data[0] == "ENEMY":
            rec_data.type = Story.Data.TYPE_ENEMY
            if rec_data.data is None:
                rec_data.data = []

            for x in range(1, data_len - 1):
                internal_data = data[x].split(':')
                if len(internal_data) < 2 or not Story._is_int(internal_data[1]):
                    raise Exception("Error while reading story data, bad data format.")

                if internal_data[0] == "EASY":
                    rec_data.data.append((Story.Data.ENEMY_EASY, int(internal_data[1])))                    
                elif internal_data[0] == "NORMAL":
                    rec_data.data.append((Story.Data.ENEMY_NORMAL, int(internal_data[1])))     
                elif internal_data[0] == "HARD":
                    rec_data.data.append((Story.Data.ENEMY_HARD, int(internal_data[1])))     

                time_data = data[-1].split(':')
                if time_data[0] == "TIME" and Story._is_int(time_data[1]):
                    rec_data.time = int(time_data[1])
                else:
                    raise Exception("Error while reading story data, no time specific.")
        elif data[0] == "UPGRADE":
            rec_data.type = Story.Data.TYPE_UPGRADE
            if data[1] == "ROCKET":
                rec_data.data = Story.Data.UPGRADE_ROCKET
            elif data[1] == "HEARTH":
                rec_data.data = Story.Data.UPGRADE_HEARTH     
            elif data[1] == "RANDOM":
                rec_data.data = Story.Data.UPGRADE_RANDOM  
            else:
                raise Exception("Error while reading story data, bad upgrade format.")
        else:
            raise Exception("Error while reading story data, bad type format.")

        return rec_data

    def close(self):
        self.file_handle.close()