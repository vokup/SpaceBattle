import arcade

class Rocket(arcade.Sprite):
        def __init__(self, file_name, world, x, y):
            super().__init__(file_name, 1)
            self.world = world
            self.center_x = x
            self.center_y = y
            self.half_width = self.width // 2

        def update(self):
            super().update()
            if self.center_x > self.world.width + self.half_width:
                self.kill()