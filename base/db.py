class Database:
    @staticmethod
    def write_score(score):
        file_handle = open('database/score.db', 'w')
        file_handle.write(str(score))
    
    @staticmethod
    def read_score():
        file_handle = open('database/score.db', 'r')
        return int(file_handle.readline())