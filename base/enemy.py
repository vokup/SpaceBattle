import arcade

class Enemy(arcade.Sprite):

    class Bullet(arcade.Sprite):
        def __init__(self, file_name, world, x, y, speed_x, speed_y):
            super().__init__(file_name, 1)
            self.world = world
            self.center_x = x
            self.center_y = y
            self.speed_x = speed_x
            self.speed_y = speed_y

        def update(self):
            self.change_x = self.speed_x * self.world.delta_time
            self.change_y = self.speed_y * self.world.delta_time
            super().update()
            if self.center_x < 0 or self.center_x > self.world.width:
                self.kill()
            if (self.center_y < 0 or self.center_y > self.world.height):
                self.kill()

    def __init__(self, world, file_name, x , y, speed_x, speed_y, alive_time):
        super().__init__(file_name, 1)
        self.world = world
        self.center_x = x
        self.center_y = y
        self.speed_x = speed_x
        self.speed_y = speed_y
        self.alive_time = alive_time
        self.init_time = self.world.elapsed

    def update(self):
        self.change_x = self.speed_x * self.world.delta_time
        self.change_y = self.speed_y * self.world.delta_time
        super().update()

    def kill(self):
        self.world.request_enemy_kill(self)
        super().kill()