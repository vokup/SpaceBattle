import random
import math
from base import Enemy

ENEMY_NORMAL_SPEED = 350
ENEMY_NORMA_SPEED_X = -200
ENEMY_NORMAL_SPEED_Y = -200
ENEMY_NORMAL_BULLET_SPEED = 1000

class EnemyNormal(Enemy):
    def __init__(self, world, x, y, alive_time):
        super().__init__(world, 'res/enemy_normal.png', x, y, ENEMY_NORMA_SPEED_X, ENEMY_NORMAL_SPEED_Y, alive_time)
        self.world_half_width = self.world.width // 2
        self.is_moving = False
        self.update_func = self.internal_update
    
    def move_to_outside_world(self):
        if self.center_x < self.world.width + self.width:
            self.request_fire_bullet()
        else:
            self.kill()
        super().update()

    def internal_update(self):
        if self.world.elapsed - self.init_time >= self.alive_time:
            self.update_func = self.move_to_outside_world
            self.move_to_outside_world()
            self.next_x = self.world.width + self.width
            self.next_y = random.randint(self.world.bottom_limit, self.world.top_limit)
            sx = self.next_x - self.center_x
            sy = self.next_y - self.center_y
            v_size = math.sqrt(sx * sx + sy * sy)
            self.speed_x = sx / v_size * ENEMY_NORMAL_SPEED
            self.speed_y = sy / v_size * ENEMY_NORMAL_SPEED
            return

        if not self.is_moving:
            self.next_x = random.randint(self.world_half_width, self.world.width)
            self.next_y = random.randint(self.world.bottom_limit, self.world.top_limit)
            sx = self.next_x - self.center_x
            sy = self.next_y - self.center_y
            v_size = math.sqrt(sx * sx + sy * sy)
            self.speed_x = sx / v_size * ENEMY_NORMAL_SPEED
            self.speed_y = sy / v_size * ENEMY_NORMAL_SPEED
            self.is_moving = True
        elif abs(self.center_x - self.next_x) <= 5 and abs(self.center_y - self.next_y) <= 5:
            self.is_moving = False

        super().update()

        self.request_fire_bullet()

    def request_fire_bullet(self):
        if random.randint(1, 100) == 50:
            sx = self.world.player.center_x - self.center_x
            sy = self.world.player.center_y - self.center_y
            v_size = math.sqrt(sx * sx + sy * sy)
            vx = (sx / v_size) * ENEMY_NORMAL_BULLET_SPEED
            vy = (sy / v_size) * ENEMY_NORMAL_BULLET_SPEED

            bullet = EnemyNormal.Bullet('res/enemy_normal_rocket.png', self.world, self.center_x, self.center_y, vx, vy)
            bullet.angle = math.atan2(sy, sx) * 180 / math.pi
            self.world.add_enemy_bullet(bullet)

    def update(self):
        self.update_func()