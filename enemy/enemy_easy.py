import random
import arcade
import math
from base import Enemy

ENEMY_EASY_SPEED_X = 200
ENYMY_EASY_SPEED_Y = 0
ENEMY_EASY_BULLET_SPEED = -400

class EnemyEasy(Enemy):

    def __init__(self, world, x, y, alive_time):
        super().__init__(world, 'res/enemy_easy.png', x, y, -ENEMY_EASY_SPEED_X, ENYMY_EASY_SPEED_Y, alive_time)
        self.world_half_width = self.world.width // 2
        self.update_func = self.move_to_play_world
    
    def move_to_play_world(self):
        if self.center_x > self.world.width:
            super().update()
        else:
            self.update_func = self.internal_update

    def internal_update(self):
        if self.world.elapsed - self.init_time >= self.alive_time:
            self.update_func = self.move_to_outside_world
            self.move_to_outside_world()
            return

        if self.center_x <= self.world_half_width or self.center_x >= self.world.width:
            self.speed_x = -self.speed_x
        super().update()
        if random.randrange(200) == 0:
            sx = ENEMY_EASY_BULLET_SPEED
            self.world.add_enemy_bullet(EnemyEasy.Bullet('res/enemy_easy_bullet.png', self.world, self.center_x, self.center_y, sx, 0))

    def move_to_outside_world(self):
        if self.center_x >= self.world.width + self.width // 2:
            self.kill()
            return

        self.speed_x = abs(self.speed_x)
        super().update()
        if random.randrange(200) == 0:
            sx = ENEMY_EASY_BULLET_SPEED
            self.world.add_enemy_bullet(EnemyEasy.Bullet('res/enemy_easy_bullet.png', self.world, self.center_x, self.center_y, sx, 0))

    def update(self):
        self.update_func()