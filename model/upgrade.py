import arcade
import random

SCALE_SPEED = 3

class Upgrade(arcade.Sprite):
    ROCKET = 0
    HEARTH = 1
    RANDOM = 2

    def __init__(self, world, upgrade_type = RANDOM):
        self.upgrade_type = upgrade_type

        if upgrade_type == Upgrade.ROCKET:
            super().__init__('res/upgrade_rocket.png')
        elif upgrade_type == Upgrade.HEARTH:
            super().__init__('res/upgrade_hearth.png')
        elif upgrade_type == Upgrade.RANDOM:
            if random.randint(0, 1) == 0:
                super().__init__('res/upgrade_rocket.png')
                self.upgrade_type = Upgrade.ROCKET
            else:
                super().__init__('res/upgrade_hearth.png')
                self.upgrade_type = Upgrade.HEARTH
        else:
            return
        
        self.world = world
        self.center_x = self.world.width // 2
        self.center_y = self.world.top_limit // 2
        self.upper_limit = self.width
        self.lower_limit = self.width // 2
        self.ratio = self.height / self.width
        self.size_dir = -1

    def update(self):
        self.width = self.width + SCALE_SPEED * self.size_dir
        self.height = self.ratio * self.width
        if self.width <= self.lower_limit or self.width >= self.upper_limit:
            self.size_dir = -self.size_dir

    def kill(self):
        super().kill()
        self.world.request_next_scene()