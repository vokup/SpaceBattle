import arcade

class Player(arcade.Sprite):
    def __init__(self, world, x , y):
        super().__init__("res/player.png", 1)
        self.world = world
        self.center_x = x
        self.center_y = y
        self.world_width = self.world.width
        self.half_width = self.width // 2
        self.half_height = self.height // 2
        self.world_half_width = self.world.width // 2
        
    def update(self):
        if self.center_x + self.half_width + self.change_x > self.world_width:
            self.center_x = self.world_width - self.half_width
        elif self.center_x - self.half_width + self.change_x < 0:
            self.center_x = self.half_width
        else:
            self.center_x += self.change_x
            self.change_x = 0
            
        if self.center_y + self.half_height + self.change_y > self.world.top_limit:
            self.center_y = self.world.top_limit - self.half_height
        elif self.center_y - self.half_height + self.change_y < self.world.bottom_limit:
            self.center_y = self.world.bottom_limit + self.half_height
        else:
            self.center_y += self.change_y
            self.change_y = 0

    def on_mouse_motion(self, x, y, dx, dy):
        self.change_x = dx
        self.change_y = dy
        #self.center_x = x
        #self.center_y = y
        #print("x: %d\ty: %d" % (x, y))