import arcade

class Hearth(arcade.Sprite):
    def __init__(self, x, y):
        super().__init__('res/hearth.png', 1)
        self.center_x = x
        self.center_y = y
        self.is_drawing = True

    # Override
    def draw(self):
        if self.is_drawing:
            super().draw()