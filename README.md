# SpaceBattle
SpaceBattle เป็นเกมแนวตะลุยอวกาศ ซึ่งผู้เล่นจะต้องทำการกำจัดศัตรูเพื่อเก็บคะแนนสะสม และผ่านด่าน โดยจะแบ่งความยากของศัตรูเป็นง่าย ปานกลางและยาก

สร้างขึ้นเพื่อเป็น Project ในวิชา Object-Oriented Programming ภายในภาควิชาวิศวกรรมคอมพิวเตอร์ มหาวิทยาลัยเกษตรศาสตร์

![Screenshot of SpaceBattle](https://storage.makeany.app/public/gitlab/space-battle.gif)

## Game Details
- **Creator:** [Thanapawee Phrachan](https://gitlab.com/vokup)
- **Language:** Python 3
- **Game Library:** Arcade
- **Design Architecture:** Model-View-Controller (MVC)
- **Delevopment Time:** 1 Month
