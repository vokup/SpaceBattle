import arcade
from world import PlayWorld, GameOverWorld

SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 720

class MainWindow(arcade.Window):

    def __init__(self, width, height):
        super().__init__(width, height, "Space Battle")
        self.set_mouse_visible(False)
        self.set_update_rate(1000 / 60)
        self.is_play_mode = True
        self.set_exclusive_mouse(self.is_play_mode)
        self.set_mouse_visible(not self.is_play_mode)
        arcade.set_background_color(arcade.color.BLACK)

        self.world = PlayWorld(self, SCREEN_WIDTH, SCREEN_HEIGHT)

    def request_play_world(self):
        self.world = PlayWorld(self, SCREEN_WIDTH, SCREEN_HEIGHT)

    def request_game_over_world(self, score):
        self.world = GameOverWorld(self, SCREEN_WIDTH, SCREEN_HEIGHT, score)

    def update(self, delta_time):
        if self.is_play_mode:
            if delta_time < 0.1:
                self.world.update(delta_time)

    def on_draw(self):
        arcade.start_render()
        self.world.on_draw()

    def on_key_press(self, symbol, modofiers):
        if symbol == arcade.key.ESCAPE:
            self.is_play_mode = not self.is_play_mode
            self.set_exclusive_mouse(self.is_play_mode)
            self.set_mouse_visible(not self.is_play_mode)

        self.world.on_key_press(symbol, modofiers)

    #def on_key_release(self, symbol, modofiers):
    #    pass

    #def on_mouse_drag(self, x, y, dx, dy, buttons, modifiers):
    #    super().on_mouse_drag(x, y, dx, dy, buttons, modifiers)

    def on_mouse_motion(self, x, y, dx, dy):
        self.world.on_mouse_motion(x, y, dx, dy)
    
    def on_mouse_press(self, x, y, button, modifiers):
        self.world.on_mouse_press(x, y, button, modifiers)

    def on_mouse_release(self, x, y, button, modifiers):
        self.world.on_mouse_release(x, y, button, modifiers)
 

def main():
    window = MainWindow(SCREEN_WIDTH, SCREEN_HEIGHT)
    arcade.run()

if __name__ == "__main__":
    main()